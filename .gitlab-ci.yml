include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

default:
  image: registry.gitlab.com/centos/kmods/ci-image:latest

variables:
  GIT_STRATEGY: none
  REPO: packages-userspace

stages:
  - prep
  - build

mock:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
      when: never
    - when: always
  parallel:
    matrix:
      - EL: [9]
        ARCH: [x86_64]
  stage: build
  script:
    - MOCK=alma+epel-$EL
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - |
      if [ -e sources ]
      then
        while read -r TYPE FILE _ HASH
        do
          if [ -z "$TYPE" ] || [ -z "$FILE" ] || [ -z "$HASH" ]
          then
            continue
          fi
          FILE=${FILE#(}
          FILE=${FILE%)}
          TYPE=${TYPE,,}
          if [ ! -e "$FILE" ]
          then
            curl -f -s -L -H Pragma: -o "./$FILE" -R --retry 5 "https://git.centos.org/sources/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE" && continue || true
            curl -f -s -S -L -H Pragma: -o "./$FILE" -R --retry 5 "https://src.fedoraproject.org/repo/pkgs/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE"
          fi
          echo "$HASH $FILE" | "${TYPE}sum" --check --status
        done < sources
      fi
    - mock -r $MOCK-$ARCH --define "dist .el$EL" --buildsrpm --spec *.spec --source . --resultdir .
    - |
      if [ -e sources ]
      then
        while read -r _ FILE _ _
        do
          if [ -z "$FILE" ]
          then
            continue
          fi
          FILE=${FILE#(}
          FILE=${FILE%)}
          if [ -e "$FILE" ]
          then
            rm -f $FILE
          fi
        done < sources
      fi
    - mock -r $MOCK-$ARCH --define "dist .el$EL" --resultdir . *.src.rpm
  artifacts:
    name: "$CI_PROJECT_NAME-mock-$ARCH"
    untracked: true
    when: always

notify:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/ && $CI_COMMIT_REF_NAME =~ /main/
  stage: prep
  script:
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - VERSION=$(rpmspec -q --srpm --queryformat='%{version}\n' *.spec)
    - RAWHIDE=$(koji list-tagged --quiet --inherit --latest rawhide $CI_PROJECT_NAME | sed -rn 's/^.*-([^-]+)-([^-]+)\s.*$/\1/p')
    - rpmdev-vercmp $VERSION $RAWHIDE || [[ $? == 11 ]] || (echo "Newer version in rawhide!"; exit 1)

lookaside:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
  stage: prep
  script:
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - |
      if [ -e sources ]
      then
        while read -r TYPE FILE _ HASH
        do
          if [ -z "$TYPE" ] || [ -z "$FILE" ] || [ -z "$HASH" ]
          then
            continue
          fi
          FILE=${FILE#(}
          FILE=${FILE%)}
          TYPE=${TYPE,,}
          if [ ! -e "$FILE" ]
          then
            curl -f -s -L -H Pragma: -I -o /dev/null -R --retry 5 "https://git.centos.org/sources/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE" && continue || true
            curl -f -s -S -L -H Pragma: -o "./$FILE" -R --retry 5 "https://src.fedoraproject.org/repo/pkgs/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE"
            echo "$HASH $FILE" | "${TYPE}sum" --check --status
            curl -f -s -S -L --cert $CBS --form "name=$CI_PROJECT_NAME" --form "hash=$TYPE" --form "${TYPE}sum=$HASH" --form "file=@$FILE" "https://git.centos.org/sources/upload_sig.cgi"
          fi
        done < sources
      fi

cbs:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
  parallel:
    matrix:
      - EL: [9]
  stage: build
  script:
    - cbs version
    - git clone --quiet --depth 1 --branch $CI_COMMIT_REF_NAME --single-branch $CI_REPOSITORY_URL .
    - NAME=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}\n' *.spec)
    - NVR=$(rpmspec -q --srpm --define "dist .el$EL" --queryformat='%{name}-%{version}-%{release}\n' *.spec)
    - 'cbs buildinfo $NVR 2>/dev/null | grep >/dev/null "State: COMPLETE" || cbs --cert=$CBS build --wait kmods$EL-$REPO-el$EL git+$CI_PROJECT_URL\#$CI_COMMIT_SHA'
    - TAGS+=(kmods${EL}-kernel-latest-release)
    - |
      if cbs list-tagged --quiet kmods${EL}-$REPO-candidate $NAME | awk '{print $1}' | grep >/dev/null "^$NVR\$"
      then
        for TAG in ${TAGS[@]}
        do
          cbs list-tagged --quiet $TAG $NAME | awk '{print $1}' | grep >/dev/null "^$NVR\$" || cbs --cert=$CBS tag-build --wait $TAG $NVR
        done
      fi
    - |
      for TAG in ${TAGS[@]}
      do
        PKGS=($(cbs list-tagged --quiet $TAG $NAME | awk '{print $1}' | rpmdev-sort | head -n -5))
        [[ ${#PKGS[@]} == 0 ]] || cbs --cert=$CBS untag-build $TAG ${PKGS[@]}
      done
